total_fuel = 0

input = File.open("input", "r")
input.each_line do |line|
  weight = line.to_f
  loop do
    fuel_req = (weight / 3.0).to_i - 2
    break if fuel_req <= 0
    total_fuel += fuel_req
    weight = fuel_req
  end
end
input.close

puts total_fuel