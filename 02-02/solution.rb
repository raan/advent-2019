# execute program in memory
# ip = instruction pointer
# a[1..3] = addresses [1..3]
def execute(program)
  halt = false
  ip = 0

  while !halt do
    if program[ip] == 99
      halt = true
    else
      a1 = program[ip+1]
      a2 = program[ip+2]
      a3 = program[ip+3]

      if program[ip] == 1
        program[a3] = program[a1] + program[a2]
      elsif program[ip] == 2
        program[a3] = program[a1] * program[a2]
      end

      ip += 4
    end
  end

  return program[0]
end

# main
memory = []
input = File.open(ARGV[0], "r")
init_state = input.readline.split(",").map { |n| n.to_i }
input.close

out = -1
noun = 0
verb = 0
done = false

while noun <= 99 && !done do
  verb = 0
  while verb <= 99 && !done do
    memory = init_state.clone
    memory[1] = noun
    memory[2] = verb
    out = execute(memory)
    if out == 19690720
      done = true
    else
      verb += 1
    end
  end

  noun += 1 if !done
end

puts 100 * noun + verb
