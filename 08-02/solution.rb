WIDTH = 25.freeze
HEIGHT = 6.freeze
LAYER_SIZE = WIDTH * HEIGHT

# file io
file = File.open("input", "r")
input = file.readline.chomp
file.close

# read layers into hash
layers = {}
ptr = 0
layer = 1
while ptr < input.length
  layers[layer] = input[ptr..ptr+LAYER_SIZE-1]
  ptr += LAYER_SIZE
  layer += 1
end

# decode
final_image = Array.new(WIDTH*HEIGHT, "2")
layers.reverse_each do |layerno, data|
  i = 0
  data.each_char do |pixel|
    if pixel != "2"
      final_image[i] = pixel
    end
    i += 1
  end
end

# print out finaly image neatly
for i in 0..HEIGHT-1
  for j in 0..WIDTH-1
    print final_image[i*WIDTH + j]
  end
  puts ""
end
