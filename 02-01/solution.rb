def execute(program)
  halt = false
  pc = 0

  while !halt do
    if program[pc] == 99
      halt = true
    else
      r1 = program[pc+1]
      r2 = program[pc+2]
      r3 = program[pc+3]

      if program[pc] == 1
        program[r3] = program[r1] + program[r2]
      elsif program[pc] == 2
        program[r3] = program[r1] * program[r2]
      end

      pc += 4
    end
  end

  return program
end

program = []

input = File.open(ARGV[0], "r")
program = input.readline.split(",").map { |n| n.to_i }
input.close

execute(program)
print program
puts
