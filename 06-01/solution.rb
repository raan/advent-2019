class Node
  attr_reader :name
  attr_accessor :parent, :children

  def initialize(obj_name)
    @name = obj_name
    @parent = nil
    @children = Array.new
  end

  def add_child(child)
    child.set_parent(self)
    @children << child
  end

  def set_parent(parent)
    @parent = parent
  end

  # returns the number of parents this node has,
  # going all the way back to the root node
  def num_parents
    count = 0
    curr = @parent
    while curr != nil
      curr = curr.parent
      count += 1
    end

    return count
  end
end

# hash to keep track of nodes we've handled
existing = {}

# read file and create nodes & relationships
input = File.open("input", "r")
input.each_line do |line|
  line = line.chomp.split(")")
  target_name = line[0]
  orbiter_name = line[1]

  if existing.key?(target_name)
    target = existing[target_name]
  else
    target = Node.new(target_name)
    existing[target_name] = target
  end

  if existing.key?(orbiter_name)
    orbiter = existing[orbiter_name]
  else
    orbiter = Node.new(orbiter_name)
    existing[orbiter_name] = orbiter
  end

  target.add_child(orbiter)
end
input.close

# sum up the num of parents each node has
count = 0
existing.each do |key, node|
  count += node.num_parents
end
puts count
