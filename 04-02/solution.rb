def valid?(num)
  prev = ""
  str = num.to_s.split("")

  # check for decreasing digits
  str.each do |digit|
    if digit.to_i < prev.to_i
      return false
    end

    prev = digit
  end

  # check for double digits
  prev = ""
  series = 0
  valid = false
  str.each do |digit|
    if digit == prev
      series += 1
    else
      # set valid = true if we have a double digit
      # but not a longer chain
      valid = true if series == 1
      series = 0
    end

    prev = digit
  end

  valid = true if series == 1
  return valid
end


i = 231832
total = 0
while i <= 767346 do
  total += 1 if valid?(i)
  i += 1
end
puts total