WIDTH = 25.freeze
HEIGHT = 6.freeze
LAYER_SIZE = WIDTH * HEIGHT

# file io
file = File.open("input", "r")
input = file.readline.chomp
file.close

# read layers into hash
layers = {}
ptr = 0
layer = 1
while ptr < input.length
  layers[layer] = input[ptr..ptr+LAYER_SIZE-1]
  ptr += LAYER_SIZE
  layer += 1
end

# find layer with least 0s
min_layer = ""
min_zero = 99999
layers.each do |layerno, data|
  matches = data.count("0")
  if matches < min_zero
    min_layer = layers[layerno]
    min_zero = matches
  end
end

puts min_layer.count("1") * min_layer.count("2")
