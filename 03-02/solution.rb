def traverse(path, wire_hash, compare_hash = {})
  here = Point.new(0, 0)
  step_count = 0
  min = 99999999

  path.each do |move|
    direction = move[0]
    magnitude = move[1, move.length].to_i

    case direction
    when "L"
      magnitude.times do
        here.x -= 1
        step_count += 1
        wire_hash[[here.x, here.y]] = step_count
        if compare_hash.key?([here.x, here.y])
          total_distance = wire_hash[[here.x, here.y]] + compare_hash[[here.x, here.y]]
          min = total_distance if total_distance < min
        end
      end
    when "R"
      magnitude.times do
        here.x += 1
        step_count += 1
        wire_hash[[here.x, here.y]] = step_count
        if compare_hash.key?([here.x, here.y])
          total_distance = wire_hash[[here.x, here.y]] + compare_hash[[here.x, here.y]]
          min = total_distance if total_distance < min
        end
      end
    when "U"
      magnitude.times do
        here.y += 1
        step_count += 1
        wire_hash[[here.x, here.y]] = step_count
        if compare_hash.key?([here.x, here.y])
          total_distance = wire_hash[[here.x, here.y]] + compare_hash[[here.x, here.y]]
          min = total_distance if total_distance < min
        end
      end
    when "D"
      magnitude.times do
        here.y -= 1
        step_count += 1
        wire_hash[[here.x, here.y]] = step_count
        if compare_hash.key?([here.x, here.y])
          total_distance = wire_hash[[here.x, here.y]] + compare_hash[[here.x, here.y]]
          min = total_distance if total_distance < min
        end
      end
    end
  end

  return min
end

# part one solution has been thrown out
Point = Struct.new(:x, :y)

input = File.open("input", "r")
path_1 = input.readline.strip.split(",")
path_2 = input.readline.strip.split(",")
input.close

# stores points that a wire has traversed,
# along with how many steps it took to get there
wire1_hash = Hash.new
wire2_hash = Hash.new

# trace path and fill hashes
traverse(path_1, wire1_hash)
puts traverse(path_2, wire2_hash, wire1_hash)

# wire1_hash.each do |key, val|
#   puts "#{key}: #{val}"
# end

# wire2_hash.each do |key, val|
#   puts "#{key}: #{val}"
# end
