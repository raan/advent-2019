total_mass = 0
module_weights = []

input = File.open("input", "r")
input.each_line do |line|
  module_weights << line.to_i
end
input.close

module_weights.each do |mass|
  total_mass += mass / 3 - 2
end

puts total_mass