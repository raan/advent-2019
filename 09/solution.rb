def get_parameter_value(program, mode, param, relative_base)
  case mode
  when 0
    program[param].to_i
  when 1
    param
  when 2
    program[relative_base + param].to_i
  end
end

def run_program(program)
  relative_base = 0
  halt = false
  ip = 0

  while !halt do
    # 0 pad the instruction to have modes for all params
    instruction = program[ip].rjust(5, "0")
    # get 2 digit opcode
    opcode = instruction[-2..-1].to_i
    # get instruction parameter modes
    mode1 = instruction[-3].to_i
    mode2 = instruction[-4].to_i
    mode3 = instruction[-5].to_i

    # get instruction params
    param1 = program[ip+1].to_i
    param2 = program[ip+2].to_i
    param3 = program[ip+3].to_i
    # change write address if relative mode is enabled
    param3 += relative_base if mode3 == 2

    # set vals depending on param modes
    val1 = get_parameter_value(program, mode1, param1, relative_base)
    val2 = get_parameter_value(program, mode2, param2, relative_base)

    case opcode
    when 99
      halt = true
    when 1
      program[param3] = (val1 + val2).to_s
      ip += 4
    when 2
      program[param3] = (val1 * val2).to_s
      ip += 4
    when 3
      print "Input: "
      input = gets.chomp
      # handle relative mode if needed
      if mode1 == 0
        program[param1] = input
      elsif mode1 == 2
        program[relative_base + param1] = input
      end
      ip += 2
    when 4
      print "Output: "
      puts val1
      ip += 2
    when 5
      ip = val1 != 0 ? val2 : ip + 3
    when 6
      ip = val1 == 0 ? val2 : ip + 3
    when 7
      program[param3] = val1 < val2 ? "1" : "0"
      ip += 4
    when 8
      program[param3] = val1 == val2 ? "1" : "0"
      ip += 4
    when 9
      relative_base += val1
      ip += 2
    end
  end

  return program[0]
end

file = File.open("input", "r")
input = file.readline.chomp.split(",")
file.close

memory = Array.new(input.length * 20) do |i|
  i < input.length ? input[i] : "0"
end

run_program(memory)

# puts memory.inspect
