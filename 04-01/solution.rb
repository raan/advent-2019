def valid?(num)
  prev = ""
  str = num.to_s.split("")

  # check for decreasing digits
  str.each do |digit|
    if digit.to_i < prev.to_i
      return false
    end

    prev = digit
  end

  # check for double digits
  prev = ""
  str.each do |digit|
    if digit == prev
      return true
    end

    prev = digit
  end

  return false
end

i = 231832
total = 0
while i <= 767346 do
  total += 1 if valid?(i)
  i += 1
end
puts total
