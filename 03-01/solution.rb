Point = Struct.new(:x, :y)

def print_state(state)
  print "." * state[0].length
  puts ".."

  state.each do |line|
    print "."
    line.each do |space|
      if space != nil
        print space
      else
        print "."
      end
    end
    puts "."
  end

  print "." * state[0].length
  puts ".."
end

def get_manhatten_distance(origin, point)
  x_dist = (point.x - origin.x).abs
  y_dist = (point.y - origin.y).abs
  return x_dist + y_dist
end

###### BEGIN CALCULATION
min_distance = 99999999 # <- lol

# File IO
input = File.open("input", "r")
wire1 = input.readline.strip.split(",")
wire2 = input.readline.strip.split(",")
input.close

##### Calculate array size
# Initialise min/maxes
min = Point.new(0, 0)
max = Point.new(0, 0)
curr = Point.new(0, 0)

# First wire
wire1.each do |move|
  direction = move[0]
  magnitude = move[1, move.length].to_i

  case direction
  when "L"
    curr.x -= magnitude
    min.x = curr.x if curr.x < min.x
  when "R"
    curr.x += magnitude
    max.x = curr.x if curr.x > max.x
  when "U"
    curr.y += magnitude
    max.y = curr.y if curr.y > max.y
  when "D"
    curr.y -= magnitude
    min.y = curr.y if curr.y < min.y
  end
end

# Second wire
curr.x = 0
curr.y = 0
wire2.each do |move|
  direction = move[0]
  magnitude = move[1, move.length].to_i

  case direction
  when "L"
    curr.x -= magnitude
    min.x = curr.x if curr.x < min.x
  when "R"
    curr.x += magnitude
    max.x = curr.x if curr.x > max.x
  when "U"
    curr.y += magnitude
    max.y = curr.y if curr.y > max.y
  when "D"
    curr.y -= magnitude
    min.y = curr.y if curr.y < min.y
  end
end

# Create 2D array and trace path of wires
path_trace = Array.new(max.y - min.y + 1) { Array.new(max.x - min.x + 1) }

# set origin
origin = Point.new(0 - min.x, 0 + max.y)
path_trace[origin.y][origin.x] = "O"
curr.x = origin.x
curr.y = origin.y

# trace first wire
wire1.each do |move|
  direction = move[0]
  magnitude = move[1, move.length].to_i

  case direction
  when "L"
    magnitude.times do
      curr.x -= 1
      path_trace[curr.y][curr.x] = "1"
    end
  when "R"
    magnitude.times do
      curr.x += 1
      path_trace[curr.y][curr.x] = "1"
    end
  when "U"
    magnitude.times do
      curr.y -= 1
      path_trace[curr.y][curr.x] = "1"
    end
  when "D"
    magnitude.times do
      curr.y += 1
      path_trace[curr.y][curr.x] = "1"
    end
  end
end

# trace second wire
curr.x = origin.x
curr.y = origin.y
wire2.each do |move|
  direction = move[0]
  magnitude = move[1, move.length].to_i

  case direction
  when "L"
    magnitude.times do
      curr.x -= 1
      if path_trace[curr.y][curr.x] == "1"
        path_trace[curr.y][curr.x] = "X"
        min_distance = [min_distance, get_manhatten_distance(origin, curr)].min
      else
        path_trace[curr.y][curr.x] = "2"
      end
    end
  when "R"
    magnitude.times do
      curr.x += 1
      if path_trace[curr.y][curr.x] == "1"
        path_trace[curr.y][curr.x] = "X"
        min_distance = [min_distance, get_manhatten_distance(origin, curr)].min
      else
        path_trace[curr.y][curr.x] = "2"
      end
    end
  when "U"
    magnitude.times do
      curr.y -= 1
      if path_trace[curr.y][curr.x] == "1"
        path_trace[curr.y][curr.x] = "X"
        min_distance = [min_distance, get_manhatten_distance(origin, curr)].min
      else
        path_trace[curr.y][curr.x] = "2"
      end
    end
  when "D"
    magnitude.times do
      curr.y += 1
      if path_trace[curr.y][curr.x] == "1"
        path_trace[curr.y][curr.x] = "X"
        min_distance = [min_distance, get_manhatten_distance(origin, curr)].min
      else
        path_trace[curr.y][curr.x] = "2"
      end
    end
  end
end

puts min_distance