def run_program(program)
  halt = false
  ip = 0

  while !halt do
    # 0 pad the instruction to have modes for all params
    instruction = program[ip].rjust(5, "0")
    # get 2 digit opcode
    opcode = instruction[-2..-1].to_i
    # get param modes
    mode1 = instruction[-3].to_i
    mode2 = instruction[-4].to_i
    mode3 = instruction[-5].to_i

    # get params
    param1 = program[ip+1].to_i
    param2 = program[ip+2].to_i
    param3 = program[ip+3].to_i

    # set vals depending on param modes
    val1 = mode1 == 0 ? program[param1].to_i : param1
    val2 = mode2 == 0 ? program[param2].to_i : param2
    val3 = mode3 == 0 ? program[param3].to_i : param3

    case opcode
    when 99
      halt = true
    when 1
      program[param3] = (val1 + val2).to_s
      ip += 4
    when 2
      program[param3] = (val1 * val2).to_s
      ip += 4
    when 3
      print "Input: "
      input = gets.chomp
      program[param1] = input
      ip += 2
    when 4
      print "Output: "
      puts val1
      ip += 2
    when 5
      ip = val1 != 0 ? val2 : ip + 3
    when 6
      ip = val1 == 0 ? val2 : ip + 3
    when 7
      program[param3] = val1 < val2 ? "1" : "0"
      ip += 4
    when 8
      program[param3] = val1 == val2 ? "1" : "0"
      ip += 4
    end
  end

  return program[0]
end

input = File.open("input", "r")
state = input.readline.chomp.split(",")
input.close
run_program(state)

# puts state.inspect
